var base_url='http://www.zonaprop.com.ar/departamento-venta-villa-crespo-publicado-hace-menos-de-5-dias-85000-132000-dolar.html'
var cheerio = require('cheerio'),
    request = require('request'),
    redis = require("redis"),
    AWS = require('aws-sdk'),
    dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
var price_regex = /(U\$S|\$)(\s)*(\d+(\.\d+)*)/;

function makeHtml(e) {
  var links = $("a",$("h4",$(".post-text",e)));

  var precio = price_regex.exec($.html($("span",$(".price",e))))[0];
  var href = "www.zonaprop.com"+links[0].attribs.href;
  var title = links[0].attribs.title;
  var img;
  try {
    img = $(".rsImg",e)[0].attribs.href;
  }catch(e) {
    console.log($.html($(".rsImg",e)))
  }
  var dept_data = $.html($(".unstyled",$(".post-text-pay",e)))

  return `<div>
            <div><img src="${img}"/></div><div><a href="http://${href}">${title}</a></div><div>${precio}</div>${dept_data}
            <br/>
         </div>`
  //console.log($.html($("a",$("h4",$(".post-text",e)))))
}
function makeReport(to,page,acum,cb) {
  var url = to.replace("##PAGE##",page);
  request({"url":url, "followRedirect":false}, function (error, response, body) {
    console.log("Processing: "+url+", "+response.statusCode);
    if(response.statusCode != 200) {
      if(cb) {
        process.nextTick(function(){
          cb(acum)
        });
      }
      console.log("Finish",error)
      console.log(acum)
    } else {
      $ = cheerio.load(body);
      var posts = $(".post");
      posts.each(function(e) {
          if(posts[e].attribs && posts[e].attribs.id) {
            acum[posts[e].attribs.id] = makeHtml(posts[e])
          }else{
            console.log(posts[e])
          }
      })
      if(!to.match(/.*##PAGE##\.html$/)) {
        to = to.replace(".html","-pagina-##PAGE##.html");
      }
      makeReport(to,page+1,acum,cb)
    }
  });
}

makeReport(base_url,1,{},function(acum) {
  console.log(acum)
  var functions = [];
  Object.keys.each(function(e){
    functions.push(function(cb) {
      var params = {
          TableName: "zonaprop_scrapped",
          Key:{
              "prop_id": e
          }
      };
      dynamodb.get(params, function(err, data) {

      });
    });
  });
})
